/*
Date: 
Author:
File Name:
Description:

*/

#include "dev.hpp"
#include "modes.hpp"

namespace fsy = std::filesystem;
bool dev::devMode = true;		//true means we want extra "dev" output. False means we do not
//bool modes::rcur_mode = false;
bool modes::rcur_mode = true;



/*

		method name:	getAllItems()
		Date:			April 12th 2019
		Author:			William Doyle
		purpose:		A proof of concept that will get all file names and all folder names in a provided folder

*/
void getAllItems(std::vector<std::string>* allItems, std::string startDir)
{

	if(modes::rcur_mode)/* recursive mode */
		for (const auto& entry : fsy::recursive_directory_iterator(startDir))
		{
			if (dev::devMode)std::cout << entry.path() << "\n";														//outputs item (file or folder ) names
			allItems->push_back(entry.path().string());																//adds item to list named allItems (created in main, every other method works on a pointer)
			std::cout << fsy::file_size(entry.path())<<"\n";														//outputs the file size (bytes i beleve)
		}
	else /* non recursive mode */
	{
		for (const auto& entry : fsy::directory_iterator(startDir))
		{
			if (dev::devMode)std::cout << entry.path() << "\n";														//outputs item (file or folder ) names
			allItems->push_back(entry.path().string());																//adds item to list named allItems (created in main, every other method works on a pointer)
			
		}
	}

}

int main(int argc, char* argv)
{
	
	std::string startDir;
	std::vector<std::string> allItems;
	startDir = "C:\\Users\\w\\Desktop\\testDir";
	getAllItems(&allItems, startDir);
	return 0;
}



