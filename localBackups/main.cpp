
/*
	Author: William Doyle
	Date: Various (around may 2019)
	Pourpous:	Program will meet criteria of challange layedout in INFO-1156 Project 3.2019.03 (v1.0.0)(1).pdf
			the main idea here is to get files from the hard drive and do analisis on them. For more info on this 
			please see the pdf or contact J Manning or Others In the CPA department of fanshawe collage. 
			The collage is located in london ontario canada.
			
			This is a redo project which will not be marked or graded. The attempt I wrote and handed in sucked so
			now I'm redoing it in my free time to ensure that I have the expreence I need to succeed in second semester.
			
			I'm going to try and keep this program simpler/cleaner than my previous attempt. Also because this is not for marks
			I am taking the liberty of writing this for my prefered OS (Linux/debian/specifically ubuntu) I will be using the g++ 
			compilor. Also I am going to try and keep this code confined to a single text file for simplicitys sake.

	To Compile: g++ main.cpp -lstdc++fs -o  main



*/



/* includes */
#include <vector>
#include <experimental/filesystem>
#include <iostream>
#include <regex>
#include <cstring>
#include <fstream>

/* defines */


namespace fsy = std::experimental::filesystem;

//structs
struct file_type_group{
/*
	Data structure file_type_group
	extent is a string representing the file extention (example .cpp)
	count is a count of how many of files with that file extention were found 
	total_size is the sum of the sizes of files with that extention
*/
	std::vector<std::string> extents;
	/* constructor -> 1 arg */
	file_type_group(std::vector<std::string> extns)
	{
		//std::cout<<"STUB: extns.size() = "<<extns.size()<<"\n";
		extents = extns;
		count.resize(extents.size());		/* resize parallel vectors to be the same length as the extents vector */
		total_size.resize(extents.size());
		
	}
	
	
	std::vector<unsigned long> count;
	std::vector<unsigned long> total_size;
	
	
	
	
	int digitcount(unsigned long num)
	{
		int length = 1;
		while(num /= 10 )
			length++;
		return length;
	}
	
	void print(){
		int ext_col_width = 0;
		int count_col_width = 0;
		int total_col_width = 0;
		for (int i = 0 ; i < extents.size(); ++i)
		{
			if (extents.at(i).size() > ext_col_width)
				ext_col_width = extents.at(i).size();
			if (digitcount(count.at(i)) > count_col_width)
				count_col_width = digitcount(count.at(i));
			if (digitcount(total_size.at(i)) > total_col_width)
				total_col_width = digitcount(total_size.at(i));
		}
		std::cout<<"Ext\tCount\tTotal Sizes\n";/* title */
		for (int i = 0 ; i < extents.size(); ++i)
		{
		/*
		
		print the table here 
		use ext_col_width, count_col_width, and total_col_width
		to keep the coloums looking nice
		
		
		*/
		
		
		std::cout<<extents.at(i)<<"\t"<<count.at(i)<<"\t"<<total_size.at(i)<<"\n";
		
		}
	}
	
	void addpath(fsy::path givenPath)
	{
		/* 
		this method will take a filesystem path and either create a new 
		file extention and make the three vectors longer OR if the fileextention has been 
		seen before it will add the provided paths filesize to the tally and increment the 
		count
		*/
		//std::cout<<"STUB: in addpath\t path = "<<givenPath.extension().string()<<"\textents.size = "<<extents.size()<<"\n";
		for (int i = 0 ; i < extents.size(); ++i)
		{
			//std::cout<<extents.at(i)<<"_________________________________________\n";
			if (givenPath.extension() == extents.at(i))
			{

				++count.at(i);
				total_size.at(i) += fsy::file_size(givenPath);
				break;
			}
		}
	}
	
	
	/* constructor -> 0 args 
	file_type_group()
	{
	
	}
	*/
	

};


struct Switches{
	bool switch_c; /*if true  search for c files (.c, .h)   */
	bool switch_plus; /*if true  search for c++ files (.cc, .cp, .cpp, .cxx, .c++, .hpp, .hxx)   */
	bool switch_j; /*if true  search for java files (.class, .j, .jad, .jar, .java, .jsp, .ser) */
	bool switch_sharp; /*if true  search for C# files (.cs, .vb, .jsl)   */
	bool switch_w; /*if true  search for web programming files (.htm, .html, .html5, .js, .jse, .jsc)   */
	bool switch_x; /*if true  search extensions using a regex expression (ie: -x “\.(docx|xlsx)”)   */
	bool switch_s; /*if true  all program file types are collected (switches c+j#w) and reported in groups. A summary of totals for each group is printed.   */
	bool switch_r; /*if true  suppress processing folders recursively.   */
	bool switch_R; /*if true  list the files in reverse order.   */
	bool switch_S; /*if true  sort the files by ascending file size – or descending if the R switch is provided.   */
	bool switch_v; /*if true  lists a detailed report by extension. Each directory should only be listed once and the number of files found in that directory and the total number of folders found for each extension. See Example of output.   */
	bool switch_h; /*if true  shows the help usage for running the program   */
	
	void print(){
		std::cout<<"SWITCHES: \n";
		std::cout<< std::boolalpha <<"\tswitch_c\t"<<switch_c<<"\n";
		std::cout<<"\tswitch_plus\t"<<switch_plus<<"\n";
		std::cout<<"\tswitch_j\t"<<switch_j<<"\n";
		std::cout<<"\tswitch_sharp\t"<<switch_sharp<<"\n";
		std::cout<<"\tswitch_w\t"<<switch_w<<"\n";
		std::cout<<"\tswitch_x\t"<<switch_x<<"\n";
		std::cout<<"\tswitch_s\t"<<switch_s<<"\n";
		std::cout<<"\tswitch_r\t"<<switch_r<<"\n";
		std::cout<<"\tswitch_R\t"<<switch_R<<"\n";
		std::cout<<"\tswitch_S\t"<<switch_S<<"\n";
		std::cout<<"\tswitch_v\t"<<switch_v<<"\n";
		std::cout<<"\tswitch_h\t"<<switch_h<<"\n";
	}
	
	Switches(): switch_c(false), switch_plus(false), switch_j(false), switch_sharp(false), switch_w(false), switch_x(false), switch_s(false), switch_r(false), switch_R(false), switch_S(false), switch_v(false), switch_h(false)
	{}
};

int main(int argc, char * argv[]){
	/* get arguments from terminal 
		1. figure out where  switches are given
		2. figure out where directory is given 
		3. figure out if a regex is required (did user turn on x?)
			if we do need a regex look for it
				if we don't have it 
					tell user of error and exit
				if we do have a regex 
					take note of where it is
		4. get the switches
		5. apply switches to struct
		6. get user provided directory and save it as a var
		7. if we have a regex switch on 
			build the regex (we took note of where it is)
				if the regex won't build 
					tell user of issue and quit
					
	
		now we should be able to print the followin information in the following form
		
		Your arguments:
			Switches: [print switchs here -> {-c+#wj}]
			Start Dir: [directory to start here -> {C:\user\Bob}]
			Regex: [regex string here | "no regex required" -> {"\.(doc|docx)"}]	
	*/
	Switches s_board;//stands for switch board because this struct is kinda like a mechanical board with a bunch of switches on it 
	std::vector<file_type_group> fileGroups; 
	std::vector<std::vector<std::string>> extGroupMap;
	
	std::string switches = "None";
	std::string directory = "";
	std::string regexString = "no regex required";/*default value */

	std::regex switchRegex("^-(.*)"); /* Starts with '-' */

	for (auto i = 0; i < argc; ++i)
	{
		std::string temp = std::string(argv[i]);
		if (std::regex_match(temp, switchRegex))
		{
			switches = temp;
			for (auto k = 0; k < switches.length(); ++k)
				if (switches.at(k) == 'x')
				{
					s_board.switch_x = true;
					regexString = std::string(argv[i+1]);
					break;  
				}
		}
		else {
			std::ifstream test(temp); 
  			if ((test) && (i!=0))
      				directory = temp;
		}
		temp = "";
	}
	if (directory == "")
		directory = fsy::current_path().string();
	std::cout<<"Your arguments:\n"<<"\tSwitches: "<<switches<<"\n\tDirectory: "<<directory<<"\n\tRegex: "<<regexString<<"\n\n";	
	
	/* lets turn on all the switches the user specified (switch_x has already been handled)*/
	for (auto i = 0; i < switches.length(); ++i)
	{
		if (switches.at(i) ==  'c'    )
			s_board.switch_c   =  true;
		else if (switches.at(i) ==   '+'   )
			s_board.switch_plus   =  true;
		else if (switches.at(i) ==   'j'   )
			s_board.switch_j   =  true;
		else if (switches.at(i) ==  '#'    )
			s_board.switch_sharp   =  true;
		else if (switches.at(i) ==    'w'  )
			s_board.switch_w   =  true;
		else if (switches.at(i) ==   's'   )
			s_board.switch_s   =  true;
		else if (switches.at(i) ==  'r'    )
			s_board.switch_r   =  true;
		else if (switches.at(i) ==   'R'   )
			s_board.switch_R   =  true;
		else if (switches.at(i) ==    'S'  )
			s_board.switch_S   =  true;
		else if (switches.at(i) ==   'v'   )
			s_board.switch_v   =  true;
		else if (switches.at(i) ==    'h'  )
			s_board.switch_h   =  true;
		else if (switches.at(i) ==   'x'   )//redundent but means x doesn't get caught in the else block
			s_board.switch_x   =  true;
		else
			if (!((i == 0)&&(switches.at(i)=='-')))
				std::cout<<"Unknown switch "<<switches.at(i)<<"\n";
	}
	
	/*
	output switches to test that they have all been flicked to the correct state
	*/
	s_board.print();
	
	/*
	
	create the structs we need and create a list of strings that that struct should be 
	responsable for 	
	
	
	example (program executed with these arguments: filesystem -+x "./(pdf)")
	we would create two structs (one for c++ files and the other for pdf files
	
	file_type_group cppGroup;
	std::vector<std::string> cppGroup_excepts = {".cc", ".cp", ".cpp", ".cxx", ".c++", ".hpp", ".hxx"};
	file_type_group pdfGroup;
	std::vector<std::string> pdfGroup_excepts = {".pdf"};
	
	this obviously is not going to work because we cannot NAME variables based on user 
	input. I will have to think of a way of handeling this.
	
	*/
	
	//name a file_type_group this will never be given any data but will be passed to the vector to allocate memory 
	
	/* the most basic situation is one where we have no arguments specifinig files to get */
	if (!(    (s_board.switch_c)   ||   (s_board.switch_plus) || (s_board.switch_j)  || (s_board.switch_sharp) || (s_board.switch_w) || (s_board.switch_x)    )  )
	{
		/* we want all our files in the same file_type_group */
		
		std::vector<std::string> temp;
		temp.push_back("AllFiles");
		
		file_type_group ftg_template(temp);
		fileGroups.push_back(ftg_template);
		extGroupMap.push_back(temp);
	}
	else{
		/* want if we dont want all of the file types? what if some switches are true? */
		//allocate space in fileGroups vector for each "group" (each extention specified by the regex gets its very own group all by itself
		if (s_board.switch_c)
		{
			
			
			std::vector<std::string> temp;
			temp.push_back(".c");
			temp.push_back(".h");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			extGroupMap.push_back(temp);
			
			/* now we have a slot to hold the C group of files and in a parallel index of extGroupMap we have a vector of file extentions that belong to that slot */
		}
		
		if (s_board.switch_plus)
		{
			
			
			std::vector<std::string> temp;
			temp.push_back(".cc");
			temp.push_back(".cp");
			temp.push_back(".cpp");
			temp.push_back(".cxx");
			temp.push_back(".c++");
			temp.push_back(".hpp");
			temp.push_back(".hxx");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_j)
		{
			
			
			std::vector<std::string> temp;
			temp.push_back(".class");
			temp.push_back(".j");
			temp.push_back(".jad");
			temp.push_back(".jar");
			temp.push_back(".java");
			temp.push_back(".jsp");
			temp.push_back(".jsc");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_sharp)
		{
			
			
			std::vector<std::string> temp;
			temp.push_back(".cs");
			temp.push_back(".vb");
			temp.push_back(".jsl");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_w)
		{
			
			
			std::vector<std::string> temp;
			temp.push_back(".htm");
			temp.push_back(".html");
			temp.push_back(".html5");
			temp.push_back(".js");
			temp.push_back(".jse");
			temp.push_back(".jsc");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_x)
		{
						
			std::vector<std::string> temp;
			temp.push_back(".NotSureHowImGoingToHandleThisSoIWontForNow");
			
			file_type_group ftg_template(temp);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		}
		
	
	}//end of else
	
	/* test by printing file groups */
	for (int i = 0 ; i < extGroupMap.size(); ++i)
	{
		for (int k = 0; k < extGroupMap.at(i).size(); ++k)
			std::cout<<extGroupMap.at(i).at(k)<<"\n";
		std::cout<<"\n";
	}
	
	
	/*
		its time to go looking for files 
			when we get one we will search through the extGroupMap to figure out what group it belongs to and then we will just
			use the addpath() method to let the struct handle that file. Regardless of if we want that file or not we will see if its 
			got a group it belongs to. If it does we will call addpath() if not we will just move on to the next file. Every file will get 
			examined regardless of if it is going to be kept.
	*/
	
	try {
		for (const auto& entry : fsy::recursive_directory_iterator(directory, fsy::directory_options::skip_permission_denied))
		{
			for (int i = 0 ; i < extGroupMap.size(); ++i)
			{
				bool breaker = false;
				for (int k = 0; k < extGroupMap.at(i).size(); ++k)
				{
					std::cout<<extGroupMap.at(i).at(k)<<"\t"<<entry.path().extension()<<"\t";
					if ( extGroupMap.at(i).at(k)  ==  entry.path().extension())
					{
						std::cout<<"match\n";
						fileGroups.at(i).addpath(entry);
						breaker = true;
						break;
					}
					else
					{
						std::cout<<"\n";
					}
				
				
				}
				if (breaker == true)
					break;
			
			
			
			}
		}
	}catch(fsy::filesystem_error e)
	{
		std::cout<<"A filesystem_error OCCURED:\n"<<e.what();
	}
	catch(...){
		std::cerr<<"We got an error while looking for files\n";
	}
				
	for (int i = 0; i < fileGroups.size(); ++i)
	{
		fileGroups.at(i).print();
		std::cout<<"\n\n\n\n";
		
	}




	std::cout<<"Made it to the end of the program...\n";
	return EXIT_SUCCESS;
}