/**
 * Program Name: PaintPanel.java
 * Purpose: shows how to respond to mouse events such as click and drag
 *          operations. This class will make use of the 
 *          MouseMotionAdapter class, which has the two methods of the 
 *          MouseMotionListener interface already implemented as empty
 *          methods. We will over-ride the mouseDragged() method and put our
 *          own code in there to get some action on the screen.
 * Coder: Bill Pulling for Sec02
 * Date: Apr 4, 2018 
 */
import javax.swing.*;//the three wise men...usually imported for any GUI
import java.awt.*;   // that is responding to events.
import java.awt.event.*;
public class PaintPanel extends JPanel
{

	//CLASS WIDE SCOPE AREA
	private Point[] pointsArray = new Point[10000];
	private int pointCounter = 0; //to keep track of how many points have been created
	
	//CONSTRUCTOR
	public PaintPanel()
	{
		//courtesy call
		super();
		//register a listener as an ANONYMOUS INNER CLASS
		this.addMouseMotionListener(new MouseMotionAdapter()
				{
			  	//OVER-RIDE the mouseDragged() method
					public void mouseDragged(MouseEvent ev)
					{
						//check that we have not filled up the array yet
						if(pointCounter < pointsArray.length)
						{
							pointsArray[pointCounter] = ev.getPoint();
							pointCounter++;
							
							//call the repaint() method to repaint the contentPane
							// of the panel each time a point is added
							repaint();
						}
					}//end method		
			
				}//end ANONYMOUS inner class				
				);
			
	}//end constructor
	
	/**
	 * Method Name: paintComponent()
	 * Purpose: repaints the current array of points to the content pane of
	 *          the JPanel. Each point will be a dot on the screen that has
	 *          dimensions of 5 pixels by 5 pixels.
	 * Accepts: an object of the Graphics class which acts as the "paint brush"
	 * Returns: nothing, void method.
	 */
	public void paintComponent(Graphics g)// g is the paint  brush
	{
		//clear the drawing area by "throwing a bucket of paint" on the canvas
		// to cover any existing dots.
		super.paintComponent(g);
		
		//loop through the array and paint the points to the screen
		for(int i = 0; i < pointCounter; i++)
		{
			//use the paint brush 'g' to paint an oval 5 pixels wide
			g.fillOval(pointsArray[i].x, pointsArray[i].y, 5, 5);
			//repaint it
			this.repaint();
		}//end loop
	}
	
	
	public static void main(String[] args)
	{
		// build the JFrame here in the main and then create an object of the
		// class and add it to the frame
		JFrame frame = new JFrame("A Simple Painting Program");
		//boilerplate
		//NOTE: we cannot use 'this' here because we're in the main
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout() );
		frame.setSize(1000, 1000);
		frame.setLocationRelativeTo(null);
		
		//create the JPanel object of PaintPanel class
		PaintPanel panel = new PaintPanel();
		//add it to this frame
		frame.add(panel);
		
		//create an Instruction label
		JLabel instrLabel = new JLabel("Hold down left mouse button and drag to draw.");
		frame.add(instrLabel, BorderLayout.SOUTH);
		
		//last line!
		frame.setVisible(true);
		
		

	}

}
