/**
 * Program Name: AnonymousClassAndLambda.java
 * Purpose: shows use of an anonymous inner class and a lambda function to handle JButton events.
 * Coder: Bill Pulling for Sec01, adapted from Deitel and Deitel, 11th edition
 * Date: March 20, 2019 
 */import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.*;

public class AnonymousClassAndLambda extends JFrame
{
	
	//constructor
	public AnonymousClassAndLambda()
	{
		super("A class that demos anonymous classes and lambda notation");
	
		//boilerplate
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout() );//ANONYMOUS layout object
		this.setSize(500,225);
		this.setLocationRelativeTo(null);
		
		JPanel mainPnl = new JPanel();
		mainPnl.setLayout(new GridLayout(2,2));
		
		//This demonstrates an anonymous class (an anonymous ActionListener is created and added to anonButton)
		JButton anonButton = new JButton("Anonymous Inner Class");
		JTextField field = new JTextField();
		mainPnl.add(field);
		mainPnl.add(anonButton);
		
		anonButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				field.setText("Anonymous class button was clicked");
			}
		
		});
		
		
		//This demonstrates lambdas
		
		JButton lambdaButton = new JButton("Lambda button");
		mainPnl.add(lambdaButton);
		lambdaButton.addActionListener((ev) -> 
		
		field.setText("The lambda button was clicked")
		
		);
		
	
		this.add(mainPnl);
	
		this.setVisible(true);
		
	}
	
	public static void main(String[] args)
	{
		new AnonymousClassAndLambda();
	}
}



