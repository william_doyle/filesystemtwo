#include <stdio.h>
#include <string.h>
#include <time.h>

#include "/usr/src/linux-headers-4.15.0-45/include/asm-generic/io.h"

//#include <asm/io.h>   /*POSIX*/

int main(){

  strut _finddatai64_t cFile;
  intptr_t hFile;
  char* filespec = "*.*";

  hFile = _findfirst64(filespec, &cFile);
  if (hFile == -1L){
    printf("No %s files in current directory!\n", filespec);
  }

  else {

    unsigned const SIZEW = 10;

    printf("RD0 HDI SYS ARC %-26s%*s NAME\n", "DATE", SIZEW, "SIZEW");
    printf("--- --- --- --- %-26s%*s ----\n", "DATE", SIZEW, "-----");
    do{
      printf("%s", (cFile.attrib&_A_RDONLY)? " Y " : " N ");
      printf("%s", (cFile.attrib&_A_HIDDEN)? " Y " : " N ");
      printf("%s", (cFile.attrib&_A_SYSTEM)? " Y " : " N ");
      printf("%s", (cFile.attrib&_A_ARCH)? " Y " : " N ");


      
      char buffer[30];
      ctime_s(buffer, sizeof(buffer), &cFile.time_write);
      buffer[strlen(buffer)-1] = 0;
      printf("%-26s", buffer);
      printf("%*llu", SIZEW, cFile.size);
      printf(" %s", cFile.name);


      
    }while(_findnext64(hFile, &cFile) == 0);

    
  }

  _findclose(hFile);

}
