/**
 * Program Name:RubberLinesPanel.java
 * Purpose: another example that uses MouseMotionListener interface.Here we draw a line by
 *         clicking and dragging. When we draw another line the original line disappears.
 * Coder: Bill Pulling for Sec02 and 03
 * Date: March 29, 2017
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RubberLinesPanel extends JPanel
{
	//CLASS SCOPE AREA
	private Point point1 = null;
	private Point point2 = null;

	//CONSTRUCTOR
	RubberLinesPanel()
	{
		//courtesy call
		super();
		
		//create a mouse motion listener using inner class
		LineListener nanny = new LineListener();
		
		//register a listener on the panel
		this.addMouseListener(nanny);
		this.addMouseMotionListener(nanny);
		
		//boilerplate
		this.setBackground(Color.BLACK);
		//call setPreferredSize and pass it an ANONYMOUS Dimension object...LOOK THESE UP
		// in the JDK Docs!
		this.setPreferredSize(new Dimension(400, 200));
		
	}//end constructor
	
	//paint() method goes here	
	public void paintComponent(Graphics g)
	{
		//call super.paint()
		super.paintComponent(g);//we're throwing a bucket of paint on the screen to
		                        //cover everything that is already there. 
														//COMMENT THIS OUT to see what effect it has on your drawing
		
		// 'g' is your paintbrush
		//set its color
		g.setColor(Color.YELLOW);		
		//draw something to the panel as long as point1 and point2 values	 ARE NOT NULL.	
		
		if(point1 != null && point2 != null)
		{
			g.drawLine(point1.x, point1.y, point2.x, point2.y);
		}		
	}//end paintComponent	
	
	//INNER CLASS HERE
	//NOTE: this inner class will implement TWO INTERFACES 
	private class LineListener implements MouseListener, MouseMotionListener
	{

		@Override
		public void mouseDragged(MouseEvent ev)
		{
			//get current position of mouse as it is being dragged across the screen
			point2 = ev.getPoint();
			//repaint the line as you drag the mouse
			repaint();
		}

		//Most of these methods are empty, except for mousePressed(), because we don't
		// use them here
		@Override
		public void mouseMoved(MouseEvent e)
		{
			// TODO Auto-generated method stub			
		}

		@Override
		public void mouseClicked(MouseEvent arg0)
		{
			// TODO Auto-generated method stub			
		}

		@Override
		public void mouseEntered(MouseEvent arg0)
		{
			// TODO Auto-generated method stub			
		}

		@Override
		public void mouseExited(MouseEvent arg0)
		{
			// TODO Auto-generated method stub			
		}

		@Override
		public void mousePressed(MouseEvent ev)
		{
			//capture the INITIAL position of the mouse where the button was pressed
			point1 = ev.getPoint();
		}

		@Override
		public void mouseReleased(MouseEvent arg0)
		{
			// TODO Auto-generated method stub			
		}		
	}//end inner class
	
	public static void main(String[] args)
	{
		// create a JFrame
		JFrame frame = new JFrame("Rubber Lines Panel Demo");		
		//create a panel and add to frame
		RubberLinesPanel panel = new RubberLinesPanel();		
		
		//boilerplate
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout() );
		frame.setSize(500,300);
		frame.setLocationRelativeTo(null);
		
		frame.add(panel, BorderLayout.CENTER);//note add this before boilerplate
																					//to see an interesting error		
		
		//pack it in!
		frame.pack();//compresses the frame as much as possible to minimize screen footprint
		//last line
		frame.setVisible(true);

	}//end main
}//end class