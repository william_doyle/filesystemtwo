/**
 * Program Name: Bouncing_Ball_V1.java
 * Purpose: a very simple animation program that shows the use of a Timer object to
 *         control the re-painting of the Ball shape on the JPanel.
 * Coder: Bill Pulling for Sec01
 * Date: March 20, 2019 
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;

public class Bouncing_Ball_V1 extends JPanel
{
	//CLASS WIDE SCOPE AREA
	private final int WIDTH = 800, HEIGHT = 700;//size of JPanel
	private final int LAG_TIME = 20; // time in milliseconds between re-paints of screen
	private Timer time;//Timer class object that will fire events every LAG_TIME interval
	private final int IMG_DIM = 100; //size of ball to be drawn
	
	private int x, y, offsetX, offsetY; //used to position ball on JPanel
	
	//constructor
	public Bouncing_Ball_V1()
	{
		//create Timer and register a listener for it.
		this.time = new Timer(LAG_TIME, new BounceListener() );
		
		//starting position for Ball will be upper left corner of JPanel
		this.x = 0; 
		this.y = 10;
		
		//distance to move on each repaint
		this.offsetX = 4; 
		this.offsetY = 4;
		
		//set preferred size of panel using an ANONYMOUS Dimension object
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT) );
		this.setBackground(Color.YELLOW);
		
		//start the timer so that it starts creating ActionEvent baby objects. 
		this.time.start();	
		
		
	}//end constructor
	
	//OVER-RIDE the JPanel's paintComponent() method
	public void paintComponent(Graphics g)//The Graphics object 'g' is your paint brush
	{
		//call super version of this method to "throw the bucket of paint onto the canvas"
		// and cover up any previous image. 
		//NOTE: try commenting this out to see the effect of not repainting.
		super.paintComponent(g);
		
		//set brush color
		g.setColor(Color.BLUE);
		//draw a circle shape
		g.fillOval(x, y,  IMG_DIM,  IMG_DIM);
	}//end paintComponent over-ride


	//INNER CLASS GOES HERE
	
	private class BounceListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// on each Timer event (every 20 milliseconds) re-calculate the co-ordinates
			// of where the ball shape will be drawn next. 
			
			//increment or decrement the x and y co-ordinates of the ball depending on 
			// where it is within the JPanel.
			
			x+= offsetX;
			y+= offsetY;
			
			//check current updated x and y to see if we are near a side wall or top or bottom
			if(x <= 0 || x >= WIDTH - IMG_DIM)
			{
				offsetX = offsetX *= -1; //changes offsetX to negative so we start moving left
			}
			if( y <= 0 || y >= HEIGHT - IMG_DIM)
			{
				offsetY = offsetY *= -1; //changes offset y to move ball UPWARDS
			}
			
			//call repaint(), which in turn calls paintComponent()
			repaint();
			
		}//end method
		
	}//end inner class
	
	public static void main(String[] args)
	{
		// create a JFrame to hold the JPanel
		JFrame frame = new JFrame("Just Follow the Bouncing Ball");
		
		//boilerplate
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new FlowLayout() );//ANONYMOUS object
		frame.setSize(1200,1000);
		frame.setLocationRelativeTo(null);
		
		//set background color of contentPane
		frame.getContentPane().setBackground(Color.BLUE);
		
		//create an ANONYMOUS object of the class and add the JPanel to the JFrame
		frame.add(new Bouncing_Ball_V1() );
		
		frame.pack();//shrinks the JFrame to the smallest size possible to conserve
		             //screen real estate. Comment it out to see its effect
		frame.setVisible(true);		

	}//end main

}//end class
