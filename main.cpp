
/*
	Author: William Doyle
	Date: Various (around may 2019)
	Pourpous:	Program will meet criteria of challange layedout in INFO-1156 Project 3.2019.03 (v1.0.0)(1).pdf
			the main idea here is to get files from the hard drive and do analisis on them. For more info on this 
			please see the pdf or contact J Manning or Others In the CPA department of fanshawe collage. 
			The collage is located in london ontario canada.
			
			This is a redo project which will not be marked or graded. The attempt I wrote and handed in sucked so
			now I'm redoing it in my free time to ensure that I have the expreence I need to succeed in second semester.
			
			I'm going to try and keep this program simpler/cleaner than my previous attempt. Also because this is not for marks
			I am taking the liberty of writing this for my prefered OS (Linux/debian/specifically ubuntu) I will be using the g++ 
			compilor. Also I am going to try and keep this code confined to a single text file for simplicitys sake.

	To Compile: g++ main.cpp -lstdc++fs -o  main
	
	
	
	LEFT TO DO:
		[] -v [verbose switch]
		[] -x [finish/fix regex (is this a compilor issue?)]
		[] add commas to numbers
		[] finish the last line of "all searches"
		[x] edit file_type_group output to include the working directoy on output 
		
		[] tidy up code (comments... remove stubs... condense where I can... bring all variable declarations to top of their respective functions)

*/



/* includes */
#include <vector>
#include <experimental/filesystem>
#include <iostream>
#include <regex>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <functional>
#include <algorithm>

/* defines */
#define EXTRA_SPACING 2	

namespace fsy = std::experimental::filesystem;

//structs
struct Switches{
	bool switch_c; /*if true  search for c files (.c, .h)   */
	bool switch_plus; /*if true  search for c++ files (.cc, .cp, .cpp, .cxx, .c++, .hpp, .hxx)   */
	bool switch_j; /*if true  search for java files (.class, .j, .jad, .jar, .java, .jsp, .ser) */
	bool switch_sharp; /*if true  search for C# files (.cs, .vb, .jsl)   */
	bool switch_w; /*if true  search for web programming files (.htm, .html, .html5, .js, .jse, .jsc)   */
	bool switch_x; /*if true  search extensions using a regex expression (ie: -x “\.(docx|xlsx)”)   */
	bool switch_s; /*if true  all program file types are collected (switches c+j#w) and reported in groups. A summary of totals for each group is printed.   */
	bool switch_r; /*if true  suppress processing folders recursively.   */
	bool switch_R; /*if true  list the files in reverse order.   */
	bool switch_S; /*if true  sort the files by ascending file size – or descending if the R switch is provided.   */
	bool switch_v; /*if true  lists a detailed report by extension. Each directory should only be listed once and the number of files found in that directory and the total number of folders found for each extension. See Example of output.   */
	bool switch_h; /*if true  shows the help usage for running the program   */
	
	void print(){
		std::cout<<"SWITCHES: \n";
		std::cout<< std::boolalpha <<"\tswitch_c\t"<<switch_c<<"\n";
		std::cout<<"\tswitch_plus\t"<<switch_plus<<"\n";
		std::cout<<"\tswitch_j\t"<<switch_j<<"\n";
		std::cout<<"\tswitch_sharp\t"<<switch_sharp<<"\n";
		std::cout<<"\tswitch_w\t"<<switch_w<<"\n";
		std::cout<<"\tswitch_x\t"<<switch_x<<"\n";
		std::cout<<"\tswitch_s\t"<<switch_s<<"\n";
		std::cout<<"\tswitch_r\t"<<switch_r<<"\n";
		std::cout<<"\tswitch_R\t"<<switch_R<<"\n";
		std::cout<<"\tswitch_S\t"<<switch_S<<"\n";
		std::cout<<"\tswitch_v\t"<<switch_v<<"\n";
		std::cout<<"\tswitch_h\t"<<switch_h<<"\n";
	}
	
	Switches(): switch_c(false), switch_plus(false), switch_j(false), switch_sharp(false), switch_w(false), switch_x(false), switch_s(false), switch_r(false), switch_R(false), switch_S(false), switch_v(false), switch_h(false)
	{}
};

int digitcount(unsigned long num)
	{
		/* a later revision will need to account for commas in long numbers REMOVE THIS COMMENT WHEN FUNCTIONALLITY IS IMPLEMENTED */
		int length = 1;
		while(num /= 10 )
			length++;
		return length;
	}

struct Line{
	std::string extents;
	unsigned long count;
	unsigned long total_size;
	
	Line(std::string ext, unsigned long tally/* didn't want to use the word "count" so I used "tally" because its an english syninom*/, unsigned long sum_sizes)
	{
		extents = ext;
		count = tally;
		total_size = sum_sizes;
	}
	
	static bool compareViaTS(Line& a,  Line& b){
		if (a.total_size > b.total_size)
		{
			return true;
		}
		return false;
	}
	
	static bool compareViaEXT(Line& a,  Line& b){
		if (a.extents > b.extents)
		{
			return true;
		}
		return false;
	}
	
	void print(int w1, int w2, int w3)
	{
		std::cout<<std::setw(w1)<<extents<<" :"<<std::setw(EXTRA_SPACING+w2)<<count<<" :"<<std::setw(EXTRA_SPACING+w3)<<total_size<<"\n";
	}
}; 

struct file_type_group{
/*
	Data structure file_type_group
	extent is a string representing the file extention (example .cpp)
	count is a count of how many of files with that file extention were found 
	total_size is the sum of the sizes of files with that extention
*/
	std::vector<std::string> extents;
	std::string groupName;
	std::string directory;
	std::vector<unsigned long> count;
	std::vector<unsigned long> total_size;
	unsigned long count_sum = 0;
	unsigned long size_sum = 0;
	
	/* constructor -> 1 arg */
	file_type_group(std::vector<std::string> extns, std::string GROUPNAME, std::string Directory)
	{
		//std::cout<<"STUB: extns.size() = "<<extns.size()<<"\n";
		groupName = GROUPNAME;
		extents = extns;
		count.resize(extents.size());		/* resize parallel vectors to be the same length as the extents vector */
		total_size.resize(extents.size());
		directory = Directory;
	}
	
	Line getGroupSummery()
	{	
		long unsigned tally = 0;
		long unsigned space = 0;
		
		for (int i = 0; i < extents.size(); ++i)
		{
			tally += count.at(i);
			space += total_size.at(i);
 		}
 		Line groupSummery(groupName, tally, space);
		return groupSummery;
	}
	
	int digitcount(unsigned long num)
	{
		/* a later revision will need to account for commas in long numbers REMOVE THIS COMMENT WHEN FUNCTIONALLITY IS IMPLEMENTED */
		int length = 1;
		while(num /= 10 )
			length++;
		return length;
	}
	
	void print(Switches s_board){
		int ext_col_width = 0;
		int count_col_width = 0;
		int total_col_width = 0;
		int count_of_distinct_extentions = 0;
		std::vector<Line> lines;
		std::string titlep1 = "Ext";			//title part one
		std::string titlep2 = "#";			//title part two
		std::string titlep3 = "Total Sizes";		//title part three
		
		std::cout<<groupName<<": "<<directory<<"\n\n";
		for (int i = 0 ; i < extents.size(); ++i)
		{
			Line temp_line(extents.at(i), count.at(i), total_size.at(i));
			lines.push_back(temp_line);
			if (count.at(i) > 0) /* Do not include data for file extentions when we never found an instance of that kind of file */
			{
				if (extents.at(i).size() > ext_col_width)
					ext_col_width = extents.at(i).size();
				if (digitcount(count.at(i)) > count_col_width)
					count_col_width = digitcount(count.at(i));
				if (digitcount(total_size.at(i)) > total_col_width)
					total_col_width = digitcount(total_size.at(i));
			}
		}
		
		if (s_board.switch_S == true)
		{
			if (s_board.switch_R == true)
				std::sort( lines.rbegin(), lines.rend(), Line::compareViaTS );
			else 
				std::sort( lines.begin(), lines.end(), Line::compareViaTS  );
		}
		else
		{
			if (s_board.switch_R == true)
				std::sort( lines.rbegin(), lines.rend(), Line::compareViaEXT);
			else 
				std::sort(lines.begin(), lines.end(), Line::compareViaEXT);
		}
		
		for (int i = 0; i < lines.size(); ++i)
		{
			extents.at(i) = lines.at(i).extents;
			count.at(i) = lines.at(i).count;
			total_size.at(i) = lines.at(i).total_size;
		}
		
		/* if the heading is longer than the largest number in that coloum adjust coloum width */
		if (titlep1.length() > ext_col_width)
			ext_col_width = titlep1.length();
		if (titlep2.length() > count_col_width)
			count_col_width = titlep2.length();
		if (titlep3.length() > total_col_width)
			total_col_width = titlep3.length();
			
		std::cout<<std::setw(ext_col_width)<<titlep1<<" :"<<std::setw(EXTRA_SPACING+count_col_width)<<titlep2<<" :"<<std::setw(EXTRA_SPACING+total_col_width)<<titlep3<<"\n";/* title */
	
		for (int i = 0; i < ext_col_width; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+count_col_width-1; ++i)
			std::cout<<"-";
		std::cout<<" ";
		std::cout<<":";
		std::cout<<" ";
		for (int i = 0; i < EXTRA_SPACING+total_col_width-1; ++i)
			std::cout<<"-";
		std::cout<<" \n";
		
		
		for (int i = 0 ; i < extents.size(); ++i)
		{
			/* print the table here use ext_col_width, count_col_width, and total_col_width to keep the coloums looking nice */
			if (count.at(i) > 0)
			{
				++count_of_distinct_extentions;
				
				lines.at(i).print(ext_col_width, count_col_width, total_col_width);
				
				/* INCREASE VALUES OF COUNT_SUM AND SIZE_SUM (note: prehaps this should be done in the main body of the struct. with current implementation these values are only useful after the output has been given. for the sake of robust code these valuse should really be kept up to date at all times) */ 
				count_sum += count.at(i);
				size_sum += total_size.at(i);
			}
		}
		
		for (int i = 0; i < ext_col_width; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+count_col_width-1; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+total_col_width-1; ++i)
			std::cout<<"-";
		std::cout<<" ";
		std::cout<<"\n";
		
		/* show number of file types THEN total of files THEN total of file sizes */
		std::cout<<std::setw(ext_col_width)<<count_of_distinct_extentions <<" :"<<std::setw(EXTRA_SPACING+count_col_width)<< count_sum <<" :"<<std::setw(EXTRA_SPACING+total_col_width)<<size_sum<<"\n";		
	}
	
	void addpath(fsy::path givenPath)
	{
		
		/* 
		this method will take a filesystem path and either create a new 
		file extention and make the three vectors longer OR if the fileextention has been 
		seen before it will add the provided paths filesize to the tally and increment the 
		count
		*/
		int i = 0;
		for (i = 0 ; i < extents.size(); ++i)
		{
			if (givenPath.extension() == extents.at(i))
			{
				++count.at(i);
				total_size.at(i) += fsy::file_size(givenPath);
				return;
			}
		}
		/* if we got here the file extension provided was not on our list... we will assume that it was not passed to this struct by mistake (were probably going after all file types)
			so we need to add that extention type to our list */
		std::cout<<"STUB: A VALUE NOT ON OUR LIST WAS GIVEN. I CAN BE FOUND IN ADDPATH().\n";
		extents.push_back(givenPath.extension());
		count.resize(extents.size());		/* resize parallel vectors to be the same length as the extents vector */
		total_size.resize(extents.size());
		
		++count.at(i);
		total_size.at(i) += fsy::file_size(givenPath);
	}
};


	
int main(int argc, char * argv[]){
	Switches s_board;/* s_board stands for "switch board" because this struct is kinda like a mechanical board with a bunch of switches on it */
	std::vector<file_type_group> fileGroups; 
	std::vector<std::vector<std::string>> extGroupMap;
	
	std::string switches = "None";
	std::string directory = "";
	std::string regexString = "no regex required";/*default value */

	std::regex switchRegex("^-(.*)"); /* Starts with '-' */
	std::regex file_regex(regexString);
	
	bool all_file_types_are_wanted = false; 
	unsigned long count_of_distinct_types = 0;
	unsigned long count_sum = 0;
	unsigned long size_sum = 0;
	
	for (auto i = 0; i < argc; ++i)
	{
		std::string temp = std::string(argv[i]);
		if (std::regex_match(temp, switchRegex))
		{
			switches = temp;
			for (auto k = 0; k < switches.length(); ++k)
				if (switches.at(k) == 'x')
				{
					s_board.switch_x = true;
					regexString = std::string(argv[i+1]);
					break;  
				}
		}
		else {
			std::ifstream test(temp); 
  			if ((test) && (i!=0))
      				directory = temp;
		}
		temp = "";
	}
	if (directory == "")
		directory = fsy::current_path().string();
//	std::cout<<"Your arguments:\n"<<"\tSwitches: "<<switches<<"\n\tDirectory: "<<directory<<"\n\tRegex: "<<regexString<<"\n\n";	
	
	/* turn on all the switches the user specified (switch_x has already been handled)*/
	for (auto i = 0; i < switches.length(); ++i)
	{
		if (switches.at(i) ==  'c'    )
			s_board.switch_c   =  true;
		else if (switches.at(i) ==   '+'   )
			s_board.switch_plus   =  true;
		else if (switches.at(i) ==   'j'   )
			s_board.switch_j   =  true;
		else if (switches.at(i) ==  '#'    )
			s_board.switch_sharp   =  true;
		else if (switches.at(i) ==    'w'  )
			s_board.switch_w   =  true;
		else if (switches.at(i) ==   's'   )
			s_board.switch_s   =  true;
		else if (switches.at(i) ==  'r'    )
			s_board.switch_r   =  true;
		else if (switches.at(i) ==   'R'   )
			s_board.switch_R   =  true;
		else if (switches.at(i) ==    'S'  )
			s_board.switch_S   =  true;
		else if (switches.at(i) ==   'v'   )
			s_board.switch_v   =  true;
		else if (switches.at(i) ==    'h'  )
		{
			s_board.switch_h   =  true;
			std::cout << "\tUsage: fileusage[-hrRsSvc + #jw(x regularexpression)][folder]\n\tswitches :\n\t\tc       C files\n\t\t+       C++ files\n\t\tj       Java files\n\t\t#       C# files\n\t\tw       Web files\n\t\ts       summary of the different categories\n\t\tx       filter with a regular expression\n\t\tr       suppress recursive processing of the folder\n\t\tR       reverse the order of the listing\n\t\tS       sort by file sizes\n\t\th       help\n\n\tfolder\n\t\tstarting folder or current folder if not provided\n";

			exit(EXIT_SUCCESS);
		}
		else if (switches.at(i) ==   'x'   )//redundent but means x doesn't get caught in the else block
			s_board.switch_x   =  true;
	/*	else
			if (!((i == 0)&&(switches.at(i)=='-')))
				std::cout<<"Unknown switch "<<switches.at(i)<<"\n";
	*/
	}
	
	/* output switches to test that they have all been flicked to the correct state */
	//s_board.print();
	
	/*
	create the structs we need and create a list of strings that that struct should be 
	responsable for 	
	*/
	
	/* the most basic situation is one where we have no arguments specifinig files to get */
	if (!(    (s_board.switch_c)   ||   (s_board.switch_plus) || (s_board.switch_j)  || (s_board.switch_sharp) || (s_board.switch_w) || (s_board.switch_x)    )  )
	{
		/* we want all our files in the same file_type_group */
		
		std::vector<std::string> temp;
		temp.push_back("AllFiles");
		
		all_file_types_are_wanted = true; 
		
		file_type_group ftg_template(temp, "AllFiles", directory);
		fileGroups.push_back(ftg_template);
		extGroupMap.push_back(temp);
	}
	else{
		/* want if we dont want all of the file types? what if some switches are true? */
		//allocate space in fileGroups vector for each "group" (each extention specified by the regex gets its very own group all by itself
		if (s_board.switch_c)
		{
			std::vector<std::string> temp;
			temp.push_back(".c");
			temp.push_back(".h");
			
			file_type_group ftg_template(temp, "C files" , directory);
			fileGroups.push_back(ftg_template);
			extGroupMap.push_back(temp);
			
			/* now we have a slot to hold the C group of files and in a parallel index of extGroupMap we have a vector of file extentions that belong to that slot */
		}
		
		if (s_board.switch_plus)
		{
			std::vector<std::string> temp;
			temp.push_back(".cc");
			temp.push_back(".cp");
			temp.push_back(".cpp");
			temp.push_back(".cxx");
			temp.push_back(".c++");
			temp.push_back(".hpp");
			temp.push_back(".hxx");
			
			file_type_group ftg_template(temp, "C++ files", directory);
			fileGroups.push_back(ftg_template);
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_j)
		{
			std::vector<std::string> temp;
			temp.push_back(".class");
			temp.push_back(".j");
			temp.push_back(".jad");
			temp.push_back(".jar");
			temp.push_back(".java");
			temp.push_back(".jsp");
			temp.push_back(".jsc");
			
			file_type_group ftg_template(temp, "Java files", directory);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		}
		
		if (s_board.switch_sharp)
		{
			std::vector<std::string> temp;
			temp.push_back(".cs");
			temp.push_back(".vb");
			temp.push_back(".jsl");
			
			file_type_group ftg_template(temp, "C# files", directory);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		}
		
		if (s_board.switch_w)
		{
			std::vector<std::string> temp;
			temp.push_back(".htm");
			temp.push_back(".html");
			temp.push_back(".html5");
			temp.push_back(".js");
			temp.push_back(".jse");
			temp.push_back(".jsc");
			
			file_type_group ftg_template(temp, "Web files", directory);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		
		}
		
		if (s_board.switch_x)
		{		
			std::vector<std::string> temp;
			temp.push_back(regexString);
			
			file_type_group ftg_template(temp, "Regex Result Files", directory);
			fileGroups.push_back(ftg_template);
			
			extGroupMap.push_back(temp);
		}
		
	
	}//end of else
	
	try {/* we will enclose all the actual file searching in a trt/catch because this block is frantic af */
		for (const auto& entry : fsy::recursive_directory_iterator(directory, fsy::directory_options::skip_permission_denied))
		{
			for (int i = 0 ; i < extGroupMap.size(); ++i)
			{
				bool breaker = false;
				for (int k = 0; k < extGroupMap.at(i).size(); ++k)
				{
					if (( extGroupMap.at(i).at(k)  ==  entry.path().extension()) && /*do not do this if path is a directory */(fsy::is_directory(entry) == false ))
					{
						/* a match has been found */
						fileGroups.at(i).addpath(entry);
						breaker = true;
						break;
					}
					else if ( (all_file_types_are_wanted == true )  && /*do not do this if path is a directory */(fsy::is_directory(entry) == false ))
					{
						fileGroups.at(i).addpath(entry);
						
						breaker == true;
						break;
					}
					
					else if ( std::regex_match( entry.path().extension().string(), file_regex) )
					{
						std::cout<<"STUB: regex_match found\n";
						fileGroups.at(fileGroups.size()-1).addpath(entry);
						
						breaker == true;
						break;
					}
				
				}
				if (breaker == true)/* IF TRUE MEANS WE NEED TO BREAK FROM THIS OUTER LOOP BECAUSE THE INNER LOOP HAS FINISHED ITS JOB */
					break;
			}
		}
	}catch(fsy::filesystem_error e)
	{
		std::cerr<<"\nA filesystem_error OCCURED:\n\n"<<e.what()<<"\n\n\n";
		exit(EXIT_FAILURE);
	}
	catch(...){
		std::cerr<<"We got an error while looking for files\n";
	}
				
	for (int i = 0; i < fileGroups.size(); ++i)
	{
		std::cout<<"\n";
		fileGroups.at(i).print(s_board);
		std::cout<<"\n";
	}
	
	if (s_board.switch_s == true)
	{
		/* BLOCK HANDLES THE SUMMERY OF FILE GROUPS IF 's' SWWITCH IS GIVEN  */
		int w1,w2,w3 = 0;/* used for keeping output formated to be 'inline' */
		
		std::string titlep1 = "Ext";			//title part one
		std::string titlep2 = "#";			//title part two
		std::string titlep3 = "Total Sizes";		//title part three
		
		/* if the heading is longer than the largest number in that coloum adjust coloum width */
		if (titlep1.length() > w1)
			w1 = titlep1.length();
		if (titlep2.length() > w2)
			w2 = titlep2.length();
		if (titlep3.length() > w3)
			w3 = titlep3.length();
	
		for (int i = 0; i < fileGroups.size(); ++i)
		{
			/*  find out greatest w1, w2, w3 so we know what the minimum padding is */
			if (fileGroups.at(i).groupName.length() > w1)
				w1 = fileGroups.at(i).groupName.length();
			if (fileGroups.at(i).digitcount(fileGroups.at(i).count_sum) > w2)
				w2 = fileGroups.at(i).digitcount(fileGroups.at(i).count_sum);
			if (fileGroups.at(i).digitcount(fileGroups.at(i).size_sum) > w3)
				w3 = fileGroups.at(i).digitcount(fileGroups.at(i).size_sum);
			
			count_sum += fileGroups.at(i).count_sum;
			size_sum += fileGroups.at(i).size_sum;
			
		}
		std::cout<<"ALL Searches: \n\n";
		
		std::cout<<std::setw(w1)<<titlep1<<" :"<<std::setw(EXTRA_SPACING+w2)<<titlep2<<" :"<<std::setw(EXTRA_SPACING+w3)<<titlep3<<"\n";/* title */
		
		for (int i = 0; i < w1; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+w2-1; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+w3-1; ++i)
			std::cout<<"-";
		std::cout<<" \n";
	
		
		for (int i = 0; i < fileGroups.size(); ++i)			
			fileGroups.at(i).getGroupSummery().print(w1,w2,w3);
		
		for (int i = 0; i < w1; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+w2-1; ++i)
			std::cout<<"-";
		std::cout<<" : ";
		for (int i = 0; i < EXTRA_SPACING+w3-1; ++i)
			std::cout<<"-";
		std::cout<<" \n";
		
		
		w1 = (digitcount(fileGroups.size()) > w1)? digitcount(fileGroups.size()):w1;
		w2 = (digitcount(count_sum) > w2)? digitcount(count_sum):w2;
		w3 = (digitcount(size_sum) > w3)? digitcount(size_sum):w3;
		/* display last line of all searches summery */
		std::cout<<std::setw(w1)<<fileGroups.size()<<" :"<<std::setw(EXTRA_SPACING+w2)<< count_sum <<" :"<<std::setw(EXTRA_SPACING+w3)<<size_sum<<"\n";		

		
	}

	std::cout<<"\n\nMade it to the end of the program...\n";
	return EXIT_SUCCESS;
}
